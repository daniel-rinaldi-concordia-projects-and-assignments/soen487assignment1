ARTISTS
----------------------------------------
curl --location --request GET "http://localhost:8080/api/artists"

curl --location --request GET "http://localhost:8080/api/artists/MetalJames"

curl --location --request POST "http://localhost:8080/api/artists" --header "Content-Type: application/json" --data-raw "{ \"nickname\": \"MetalJames\", \"firstName\": \"James\", \"lastName\": \"Hetfield\", \"description\": \"Lead vocalist for Metallica.\"}"

curl --location --request PUT "http://localhost:8080/api/artists" --header "Content-Type: application/json" --data-raw "{\"nickname\": \"MetalJames\", \"description\": \"Lead vocalist for Metallica. Also a Badass.\"}"

curl --location --request DELETE "http://localhost:8080/api/artists/MetalJames"
----------------------------------------
ALBUMS
----------------------------------------
curl --location --request GET "http://localhost:8081/api/albums"

curl --location --request GET "http://localhost:8081/api/albums/a1"

curl --location --request POST "http://localhost:8081/api/albums" --header "Content-Type: application/json" --data-raw "{\"isrc\": \"a1\", \"title\": \"Black Album\", \"description\": \"Metallica\'s self entitled black album.\", \"releaseYear\": 1991, \"artistNickname\": \"MetalJames\"}"

curl --location --request PUT "http://localhost:8081/api/albums" --header "Content-Type: application/json" --data-raw "{\"isrc\": \"a1\", \"title\": \"Metallica\"}"

curl --location --request DELETE "http://localhost:8081/api/albums/a1"