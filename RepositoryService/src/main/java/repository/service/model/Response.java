package repository.service.model;

public class Response
{
	private int status;
	private String message;
	private Object data;

	public Response()
	{
		status = 200;
	}

	public Response(int status, String message, Object data)
	{
		this.status = status;
		this.message = message;
		this.data = data;
	}

	public int getStatus()
	{
		return status;
	}
	public void setStatus(int status)
	{
		this.status = status;
	}

	public String getMessage()
	{
		return message;
	}
	public void setMessage(String message)
	{
		this.message = message;
	}

	public Object getData()
	{
		return data;
	}
	public void setData(Object data)
	{
		this.data = data;
	}

	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		sb.append(status);
		if (message != null) sb.append(" : ").append(message);
		if (data != null) sb.append('\n').append(data);
		
		return sb.toString();
	}
}
