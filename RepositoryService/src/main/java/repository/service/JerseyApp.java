package repository.service;

import java.io.IOException;
import java.net.URI;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

public class JerseyApp
{
	public static void main(String[] args) throws IOException
	{
		final ResourceConfig resourceConfig = new ResourceConfig().packages(true, "repository.service.webservices");
		final HttpServer httpServer = GrizzlyHttpServerFactory.createHttpServer(URI.create(Constants.JERSEY_URI_PATH), resourceConfig);
		
		System.out.println(String.format("Jersey app started with WADL available at " + "%sapplication.wadl\nHit enter to stop it...", Constants.JERSEY_URI_PATH));
		System.in.read();
		
		httpServer.shutdownNow();
	}
}
