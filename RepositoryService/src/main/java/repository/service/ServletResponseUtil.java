package repository.service;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import jakarta.ws.rs.core.MediaType;
import repository.service.model.Response;

public final class ServletResponseUtil
{
	private ServletResponseUtil()
	{
		// do not instantiate
	}
	
	public static void sendText(HttpServletResponse response, int httpStatusCode, String customMessage, Object data) throws IOException
	{
		response.setStatus(httpStatusCode);
		sendText(response, customMessage, data);
	}
	public static void sendText(HttpServletResponse response, String customMessage, Object data) throws IOException
	{
		Response strResponse = new Response(response.getStatus(), customMessage, data);
		sendText(response, strResponse);
	}
	public static void sendText(HttpServletResponse response, Response responseObj) throws IOException
	{
		response.setStatus(responseObj.getStatus());
		response.setContentType(MediaType.TEXT_PLAIN);
		response.setCharacterEncoding("UTF-8");
		
		PrintWriter out = response.getWriter();
		out.print(responseObj == null ? "" : responseObj.toString());
		out.flush();
		out.close();
	}
}
