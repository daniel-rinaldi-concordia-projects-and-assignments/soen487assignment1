package repository.service.webservices;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import jakarta.inject.Singleton;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import repository.core.IAlbumRepository;
import repository.impl.AlbumRepository;
import repository.model.Album;
import repository.service.model.Response;

@Path("albums")
@Singleton
public class AlbumService
{
	private IAlbumRepository albumRepository;
	
	public AlbumService()
	{
		albumRepository = new AlbumRepository();
	}
	
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String getAll()
	{
		List<Album> albums = albumRepository.findAll();
		return new Response(HttpServletResponse.SC_OK, null, albums == null ? "" : albums.stream().map(Object::toString).collect(Collectors.joining("\n"))).toString();
	}
	
	@GET
	@Path("{isrc}")
	@Produces(MediaType.TEXT_PLAIN)
	public String get(@PathParam("isrc") String isrc)
	{
		Album album = null;
		try
		{
			album = albumRepository.find(isrc).orElse(null);
		}
		catch (IllegalArgumentException e)
		{
			return new Response(HttpServletResponse.SC_BAD_REQUEST, e.getMessage(), null).toString();
		}
		
		if (album == null)
			return new Response(HttpServletResponse.SC_NOT_FOUND, "Resource was not found using the provided identifier.", null).toString();
		else
			return new Response(HttpServletResponse.SC_OK, null, album.toString()).toString();
	}

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String create(String albumJSON)
	{
		try
		{
			ObjectMapper objectMapper = new ObjectMapper();
			Album album = objectMapper.readValue(albumJSON, Album.class);
			albumRepository.add(album);
		}
		catch (IllegalArgumentException | JsonProcessingException e)
		{
			return new Response(HttpServletResponse.SC_BAD_REQUEST, e.getMessage()+"\nResource was not created.", null).toString();
		}
		
		return new Response(HttpServletResponse.SC_OK, "The resource was created successfully.", null).toString();
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String update(String albumJSON)
	{
		Album oldAlbum;
		try
		{
			ObjectMapper objectMapper = new ObjectMapper();
			Album album = objectMapper.readValue(albumJSON, Album.class);
			oldAlbum = albumRepository.update(album);
		}
		catch (IllegalArgumentException | JsonProcessingException e)
		{
			return new Response(HttpServletResponse.SC_BAD_REQUEST, e.getMessage()+"\nResource was not saved.", null).toString();
		}
		
		if (oldAlbum == null)
			return new Response(HttpServletResponse.SC_NOT_FOUND, "Resource was not found using the provided identifier.", null).toString();
		else
			return new Response(HttpServletResponse.SC_OK, "The resource was updated successfully.", null).toString();
	}

	@DELETE
	@Path("{isrc}")
	@Produces(MediaType.TEXT_PLAIN)
	public String delete(@PathParam("isrc") String isrc)
	{
		boolean success = false;
		try
		{
			success = albumRepository.delete(isrc);
		}
		catch (IllegalArgumentException e)
		{
			return new Response(HttpServletResponse.SC_BAD_REQUEST, e.getMessage()+"\nResource was not modified.", null).toString();
		}
		
		if (!success)
			return new Response(HttpServletResponse.SC_NOT_FOUND, "Resource was not found using the provided identifier.", null).toString();
		else
			return new Response(HttpServletResponse.SC_OK, "The resource was removed successfully.", null).toString();
	}
}
