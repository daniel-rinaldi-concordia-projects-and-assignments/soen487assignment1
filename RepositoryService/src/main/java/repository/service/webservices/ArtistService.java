package repository.service.webservices;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import repository.core.IArtistRepository;
import repository.impl.ArtistRepository;
import repository.model.Artist;
import repository.service.Constants;
import repository.service.ServletResponseUtil;
import repository.service.model.Response;

@WebServlet(Constants.BASE_API_PATH + "artists/*")
public class ArtistService extends HttpServlet
{
	private static final long serialVersionUID = 1L;
	
	private IArtistRepository artistRepository;
	
	public ArtistService()
	{
		super();
		artistRepository = new ArtistRepository();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		String pathInfo = request.getRequestURI();
		String[] pathParameters = pathInfo == null ? null : pathInfo.split("/");
		String nickname = pathParameters == null || pathParameters.length < 4 ? null : pathParameters[3];
		
		if (nickname != null)
		{
			nickname = nickname.trim();
			ServletResponseUtil.sendText(response, get(response, nickname));
		}
		else
			ServletResponseUtil.sendText(response, getAll(response));
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		ObjectMapper objectMapper = new ObjectMapper();
		Artist artist = objectMapper.readValue(request.getReader(), Artist.class);
		
		ServletResponseUtil.sendText(response, create(response, artist));
	}

	@Override
	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		ObjectMapper objectMapper = new ObjectMapper();
		Artist artist = objectMapper.readValue(request.getReader(), Artist.class);
		
		ServletResponseUtil.sendText(response, update(response, artist));
	}

	@Override
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		String pathInfo = request.getRequestURI();
		String[] pathParameters = pathInfo == null ? null : pathInfo.split("/");
		String nickname = pathParameters == null || pathParameters.length < 4 ? null : pathParameters[3];
		
		ServletResponseUtil.sendText(response, delete(response, nickname));
	}
	
	public Response getAll(HttpServletResponse response)
	{
		List<Artist> artists = artistRepository.findAll();
		return new Response(HttpServletResponse.SC_OK, null, artists == null ? "" : artists.stream().map(Object::toString).collect(Collectors.joining("\n")));
	}
	
	public Response get(HttpServletResponse response, String nickname)
	{
		Artist artist = null;
		try
		{
			artist = artistRepository.find(nickname).orElse(null);
		}
		catch (IllegalArgumentException e)
		{
			return new Response(HttpServletResponse.SC_BAD_REQUEST, e.getMessage(), null);
		}
		
		if (artist == null)
			return new Response(HttpServletResponse.SC_NOT_FOUND, "Resource was not found using the provided identifier.", null);
		else
			return new Response(HttpServletResponse.SC_OK, null, artist.toString());
	}

	public Response create(HttpServletResponse response, Artist artist)
	{
		try
		{
			artistRepository.add(artist);
		}
		catch (IllegalArgumentException e)
		{
			return new Response(HttpServletResponse.SC_BAD_REQUEST, e.getMessage()+"\nResource was not created.", null);
		}
		
		return new Response(HttpServletResponse.SC_OK, "The resource was created successfully.", null);
	}

	public Response update(HttpServletResponse response, Artist artist)
	{
		Artist oldArtist;
		try
		{
			oldArtist = artistRepository.update(artist);
		}
		catch (IllegalArgumentException e)
		{
			return new Response(HttpServletResponse.SC_BAD_REQUEST, e.getMessage()+"\nResource was not saved.", null);
		}
		
		if (oldArtist == null)
			return new Response(HttpServletResponse.SC_NOT_FOUND, "Resource was not found using the provided identifier.", null);
		else
			return new Response(HttpServletResponse.SC_OK, "The resource was updated successfully.", null);
	}

	public Response delete(HttpServletResponse response, String nickname)
	{
		boolean success = false;
		try
		{
			success = artistRepository.delete(nickname);
		}
		catch (IllegalArgumentException e)
		{
			return new Response(HttpServletResponse.SC_BAD_REQUEST, e.getMessage()+"\nResource was not modified.", null);
		}
		
		if (!success)
			return new Response(HttpServletResponse.SC_NOT_FOUND, "Resource was not found using the provided identifier.", null);
		else
			return new Response(HttpServletResponse.SC_OK, "The resource was removed successfully.", null);
	}
}
