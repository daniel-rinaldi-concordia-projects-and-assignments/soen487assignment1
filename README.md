# SOEN 487 Assignment 1

This assignment consists of the following sections:  
1. The Repository Core  
2. The Repository Implementation  
3. The Repository Service  
4. The Client  
5. Source Control + Documentation  

See `documentation/SOEN487_Assignment_1.pdf` for more details.

<br/>

## Table of Contents
- [SOEN 487 Assignment 1](#soen-487-assignment-1)
  - [Table of Contents](#table-of-contents)
  - [Authors](#authors)
  - [Getting Started](#getting-started)
    - [Integrated Development Environment (IDE) Setup](#integrated-development-environment-ide-setup)
  - [Grading Scheme](#grading-scheme)

<br/>

## Authors
- **Daniel Rinaldi - 40010464**

<br/>

## Getting Started

### Integrated Development Environment (IDE) Setup
Eclipse (STS) is the IDE used in development. This project is a basic dynamic web project that uses maven configuration. 
1. After cloning this repository, in order to setup the projects properly in Eclipse, switch to Java EE perspective and right click in the project explorer view and select `import > import...`. In the popup dialog, search for and select `Existing Projects into Workspace`. In the next dialog window select the project folders and press finish. 
2. Right click <u>on one of the projects</u> in the project explorer view and click `Configure > Convert to Maven Project` or if they are already maven projects click `Maven > Update Project...`. In the dialog that pops up select all the projects and hit finish. Now the projects should have maven configuration setup properly.  
3. Add a new Java run configuration for each project. Go to `Run > Run Configurations` and create a Java Application configuration for each project.

### IDE Apache Tomcat Server Setup
1. Add the server run configuration. Go to `Run > Run Configurations` and create a new tomcat server configuration.
2. For some reason, when creating the server configuration, there is a dash (illegal) added as the default admin port for tomcat. Open the Servers view by going to `Window > Show View > Servers`. Double click on the tomcat sever to be able to modify it. In the Overview tab, under the `Ports` accordian menu, change `Tomcat admin port` to 0 or some other number other than a `-`. Save the file (`ctrl+s`) and close it.
3. You need to make sure that you have added tomcat server library to the classpath. Right click <u>on the project (RepositoryService)</u> in the project explorer view and click on `properties`. In the project properties window, select `Java Build Path`, click on the `Libraries` tab and make sure that Aapache Tomcat v9.0 is listed under Classpath. If not you will need to add it. Select "Classpath" then click on the `Add Library` button on the right and select `Server Runtime > Aapache Tomcat v9.0`.
4. I use `/` as the context root for this web project. So you need to right click <u>on the project</u> in the project explorer view and click on `properties`. In the project properties window, select `Web Project Settings` in the left hand menu. Change the context root property to `/`.
5. Add the web project to the server. Open the Servers view by going to `Window > Show View > Servers`. Right click on the tomcat sever and select `Add and Remove...`. Add the web project to the configured list of projects and press finish.
6. Run the run configuration that you had setup in step 1. If everything was done correctly, you should gett a message in the console saying something like : `INFO: Server startup in [5982] milliseconds`. Next open a web browser and navigate to `http://localhost:{port_number}/`.

<br/>

## Grading Scheme
| Grading Criteria               | Points    |  
| :----------------------------- | --------: |  
| The Core Layer                 | 5         |  
| The Repository Business Layer  | 15        |  
| The Artists Servlet            | 15        |  
| The Albums Service             | 15        |  
| The Client Code                | 10        |  
| The Console Class              | 10        |  
| Error Handling (client, server)| 10        |  
| Handling Concurrency           | 5         |  
| Correct Architecture and Design| 10        |  
| Using curl                     | 5         |  
| Total                          | 100       |  

<br/>
