package repository.core;

import java.util.List;
import java.util.Optional;

public interface IRepository<I, T>
{
	Optional<T> find(I identifier) throws IllegalArgumentException;
	List<T> findAll();
	T add(T obj) throws IllegalArgumentException;
	T update(T modifiedObj) throws IllegalArgumentException;
	boolean delete(I identifier) throws IllegalArgumentException;
}
