package repository.core;

import repository.model.Artist;

public interface IArtistRepository extends IRepository<String, Artist>
{
	
}
