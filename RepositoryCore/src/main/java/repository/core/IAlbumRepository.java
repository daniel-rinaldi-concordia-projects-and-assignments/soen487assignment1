package repository.core;

import repository.model.Album;

public interface IAlbumRepository extends IRepository<String, Album>
{
	
}