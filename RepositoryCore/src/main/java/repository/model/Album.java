package repository.model;

public class Album
{
	private Long id;
	private String isrc;
	private String title;
	private String description;
	private Integer releaseYear;
	private String artistNickname;
	
	public Album()
	{
		//
	}
	
	public Album(Album other)
	{
		this.id = other.id;
		this.isrc = other.isrc;
		this.title = other.title;
		this.description = other.description;
		this.releaseYear = other.releaseYear;
		this.artistNickname = other.artistNickname;
	}

	public Long getId()
	{
		return id;
	}
	public void setId(Long id)
	{
		this.id = id;
	}

	public String getIsrc()
	{
		return isrc;
	}
	public void setIsrc(String isrc)
	{
		this.isrc = isrc;
	}

	public String getTitle()
	{
		return title;
	}
	public void setTitle(String title)
	{
		this.title = title;
	}

	public String getDescription()
	{
		return description;
	}
	public void setDescription(String description)
	{
		this.description = description;
	}

	public Integer getReleaseYear()
	{
		return releaseYear;
	}
	public void setReleaseYear(Integer releaseYear)
	{
		this.releaseYear = releaseYear;
	}

	public String getArtistNickname()
	{
		return artistNickname;
	}
	public void setArtistNickname(String artistNickname)
	{
		this.artistNickname = artistNickname;
	}

	@Override
	public String toString()
	{
		return "Album [isrc=" + isrc + ", title=" + title + ", description=" + description
		        + ", releaseYear=" + releaseYear + ", artistNickname=" + artistNickname + "]";
	}
}
