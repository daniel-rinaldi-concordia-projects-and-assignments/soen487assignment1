package repository.model;

public class Artist
{
	private Long id;
	private String nickname;
	private String firstName;
	private String lastName;
	private String description;
	
	public Artist()
	{
		//
	}
	
	public Artist(Artist other)
	{
		this.id = other.id;
		this.nickname = other.nickname;
		this.firstName = other.firstName;
		this.lastName = other.lastName;
		this.description = other.description;
	}

	public Long getId()
	{
		return id;
	}
	public void setId(Long id)
	{
		this.id = id;
	}
	
	public String getNickname()
	{
		return nickname;
	}
	public void setNickname(String nickname)
	{
		this.nickname = nickname;
	}
	
	public String getFirstName()
	{
		return firstName;
	}
	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}
	
	public String getLastName()
	{
		return lastName;
	}
	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}
	
	public String getDescription()
	{
		return description;
	}
	public void setDescription(String description)
	{
		this.description = description;
	}
	
	@Override
	public String toString()
	{
		return "Artist [nickname=" + nickname + ", firstName=" + firstName + ", lastName=" + lastName
		        + ", description=" + description + "]";
	}
}
