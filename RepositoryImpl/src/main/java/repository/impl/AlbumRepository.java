package repository.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import repository.core.IAlbumRepository;
import repository.model.Album;

public class AlbumRepository implements IAlbumRepository
{
	private static long autoId = -1l; 
	private List<Album> albumsDatastore = new ArrayList<Album>();
	
	public AlbumRepository()
	{
		//
	}

	@Override
	public Optional<Album> find(String isrc) throws IllegalArgumentException
	{
		if (isrc == null || isrc.isBlank()) throw new IllegalArgumentException("identifier must not be empty.");
		return albumsDatastore.stream().filter(a -> isrc.equals(a.getIsrc())).findFirst();
	}

	@Override
	public List<Album> findAll()
	{
		return albumsDatastore;
	}

	@Override
	public Album add(Album album) throws IllegalArgumentException
	{
		if (album == null) throw new IllegalArgumentException("value must not be null.");
		if (album.getIsrc() == null || album.getIsrc().isBlank()) throw new IllegalArgumentException("identifier must not be empty.");
		if (albumsDatastore.stream().filter(a -> album.getIsrc().equals(a.getIsrc())).findFirst().orElse(null) != null) throw new IllegalArgumentException("A resource with this identifier already exists.");
		
		album.setId(++autoId);
		albumsDatastore.add(album);
		return album;
	}

	@Override
	public Album update(Album album) throws IllegalArgumentException
	{
		if (album == null) throw new IllegalArgumentException("value must not be null.");
		if (album.getIsrc() == null || album.getIsrc().isBlank()) throw new IllegalArgumentException("identifier must not be empty.");
		
		Album replacedAlbum = null;
		for (int i = 0; i < albumsDatastore.size(); ++i)
		{
			if (album.getIsrc().equals(albumsDatastore.get(i).getIsrc()))
			{
				Album newAlbum = new Album(albumsDatastore.get(i));
				newAlbum.setId(album.getId());
				newAlbum.setIsrc(album.getIsrc());
				if (album.getTitle() != null) newAlbum.setTitle(album.getTitle());
				if (album.getDescription() != null) newAlbum.setDescription(album.getDescription());
				if (album.getReleaseYear() != null) newAlbum.setReleaseYear(album.getReleaseYear());
				if (album.getArtistNickname() != null) newAlbum.setArtistNickname(album.getArtistNickname());
				replacedAlbum = albumsDatastore.set(i, newAlbum);
			}
		}
		return replacedAlbum;
	}

	@Override
	public boolean delete(String isrc) throws IllegalArgumentException
	{
		if (isrc == null || isrc.isBlank()) throw new IllegalArgumentException("identifier must not be empty.");
		return albumsDatastore.removeIf(a -> isrc.equals(a.getIsrc()));
	}
}
