package repository.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import repository.core.IArtistRepository;
import repository.model.Artist;

public class ArtistRepository implements IArtistRepository
{
	private static long autoId = -1l; 
	private List<Artist> artistsDatastore = new ArrayList<Artist>();
	
	public ArtistRepository()
	{
		//
	}
	
	@Override
	public Optional<Artist> find(String nickname) throws IllegalArgumentException
	{
		if (nickname == null || nickname.isBlank()) throw new IllegalArgumentException("identifier must not be empty.");
		return artistsDatastore.stream().filter(a -> nickname.equals(a.getNickname())).findFirst();
	}

	@Override
	public List<Artist> findAll()
	{
		return artistsDatastore;
	}

	@Override
	public Artist add(Artist artist) throws IllegalArgumentException
	{
		if (artist == null) throw new IllegalArgumentException("value must not be null.");
		if (artist.getNickname() == null || artist.getNickname().isBlank()) throw new IllegalArgumentException("identifier must not be empty.");
		if (artistsDatastore.stream().filter(a -> artist.getNickname().equals(a.getNickname())).findFirst().orElse(null) != null) throw new IllegalArgumentException("A resource with this identifier already exists.");
		
		artist.setId(++autoId);
		artistsDatastore.add(artist);
		return artist;
	}

	@Override
	public Artist update(Artist artist) throws IllegalArgumentException
	{
		if (artist == null) throw new IllegalArgumentException("value must not be null.");
		if (artist.getNickname() == null || artist.getNickname().isBlank()) throw new IllegalArgumentException("identifier must not be empty.");
		
		Artist replacedArtist = null;
		for (int i = 0; i < artistsDatastore.size(); ++i)
		{
			if (artist.getNickname().equals(artistsDatastore.get(i).getNickname()))
			{
				Artist newArtist = new Artist(artistsDatastore.get(i));
				newArtist.setId(artist.getId());
				newArtist.setNickname(artist.getNickname());
				if (artist.getFirstName() != null) newArtist.setFirstName(artist.getFirstName());
				if (artist.getLastName() != null) newArtist.setLastName(artist.getLastName());
				if (artist.getDescription() != null) newArtist.setDescription(artist.getDescription());
				replacedArtist = artistsDatastore.set(i, newArtist);
			}
		}
		return replacedArtist;
	}

	@Override
	public boolean delete(String nickname) throws IllegalArgumentException
	{
		if (nickname == null || nickname.isBlank()) throw new IllegalArgumentException("identifier must not be empty.");
		return artistsDatastore.removeIf(a -> nickname.equals(a.getNickname()));
	}
}
