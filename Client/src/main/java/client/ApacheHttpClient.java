package client;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Scanner;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class ApacheHttpClient
{
	private static final String ALBUM_SERVICE_API = "http://localhost:8081/api/albums";
	private static final String ARTIST_SERVICE_API = "http://localhost:8080/api/artists";

	private static CloseableHttpClient client;
	private static Scanner scanner;

	public static void main(String[] args) throws IOException
	{
		System.out.println("APPLICATION START...");

		client = HttpClients.createDefault();
		scanner = new Scanner(System.in);

		Menu mainMenu = new Menu();
		Menu artistMenu = new Menu(mainMenu);
		Menu albumMenu = new Menu(mainMenu);

		mainMenu.AddOption("artists service", () -> artistMenu.prompt());
		mainMenu.AddOption("albums service", () -> albumMenu.prompt());

		artistMenu.AddOption("list artists", () -> listArtists());
		artistMenu.AddOption("get artist details", () -> getArtist());
		artistMenu.AddOption("add artist", () -> addArtist());
		artistMenu.AddOption("update artist", () -> updateArtist());
		artistMenu.AddOption("delete artist", () -> deleteArtist());

		albumMenu.AddOption("list albums", () -> listAlbums());
		albumMenu.AddOption("get album details", () -> getAlbum());
		albumMenu.AddOption("add album", () -> addAlbum());
		albumMenu.AddOption("update album", () -> updateAlbum());
		albumMenu.AddOption("delete album", () -> deleteAlbum());

		mainMenu.prompt();

		client.close();

		System.out.println("\nAPPLICATION EXIT.");
	}

	private static void listArtists()
	{
		HttpGet request = new HttpGet(ARTIST_SERVICE_API);
		try
		{
			HttpResponse response = client.execute(request);
			if (response.getEntity() != null) response.getEntity().getContent().transferTo(System.out);
			System.out.println();
		}
		catch (IOException e)
		{
			System.out.println(e.getMessage());
		}
	}

	private static void getArtist()
	{
		System.out.print("> Artist's nickname : ");
		String nickname = scanner.nextLine();

		try
		{
			URIBuilder uriBuilder = new URIBuilder(ARTIST_SERVICE_API);
			uriBuilder.setPath(uriBuilder.getPath()+"/"+nickname);
			HttpGet request = new HttpGet(uriBuilder.build());

			HttpResponse response = client.execute(request);
			if (response.getEntity() != null) response.getEntity().getContent().transferTo(System.out);
			System.out.println();
		}
		catch (IOException | URISyntaxException e)
		{
			System.out.println(e.getMessage());
		}
	}

	private static void addArtist()
	{
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode rootNode = mapper.createObjectNode();

		System.out.print("> Artist's nickname : ");
		rootNode.put("nickname", scanner.nextLine());

		System.out.print("> Artist's firstName : ");
		rootNode.put("firstName", scanner.nextLine());

		System.out.print("> Artist's lastName : ");
		rootNode.put("lastName", scanner.nextLine());

		System.out.print("> Artist's description : ");
		rootNode.put("description", scanner.nextLine());

		try
		{
			HttpPost request = new HttpPost(ARTIST_SERVICE_API);
			request.setEntity(new StringEntity(mapper.writeValueAsString(rootNode)));
			request.setHeader("Content-type", "application/json");

			HttpResponse response = client.execute(request);
			if (response.getEntity() != null) response.getEntity().getContent().transferTo(System.out);
			System.out.println();
		}
		catch (IOException e)
		{
			System.out.println(e.getMessage());
		}
	}

	private static void updateArtist()
	{
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode rootNode = mapper.createObjectNode();

		System.out.print("> Artist's nickname: ");
		String nickname = scanner.nextLine();
		if (!nickname.isBlank()) rootNode.put("nickname", nickname);

		System.out.print("> Artist's firstName (ENTER to skip): ");
		String firstName = scanner.nextLine();
		if (!firstName.isEmpty()) rootNode.put("firstName", firstName);

		System.out.print("> Artist's lastName (ENTER to skip): ");
		String lastName = scanner.nextLine();
		if (!lastName.isEmpty()) rootNode.put("lastName", lastName);

		System.out.print("> Artist's description (ENTER to skip): ");
		String description = scanner.nextLine();
		if (!description.isEmpty()) rootNode.put("description", description);

		try
		{
			HttpPut request = new HttpPut(ARTIST_SERVICE_API);
			request.setEntity(new StringEntity(mapper.writeValueAsString(rootNode)));
			request.setHeader("Content-type", "application/json");

			HttpResponse response = client.execute(request);
			if (response.getEntity() != null) response.getEntity().getContent().transferTo(System.out);
			System.out.println();
		}
		catch (IOException e)
		{
			System.out.println(e.getMessage());
		}
	}

	private static void deleteArtist()
	{
		System.out.print("> Artist's nickname : ");
		String nickname = scanner.nextLine();

		try
		{
			URIBuilder uriBuilder = new URIBuilder(ARTIST_SERVICE_API);
			uriBuilder.setPath(uriBuilder.getPath()+"/"+nickname);
			HttpDelete request = new HttpDelete(uriBuilder.build());

			HttpResponse response = client.execute(request);
			if (response.getEntity() != null) response.getEntity().getContent().transferTo(System.out);
			System.out.println();
		}
		catch (IOException | URISyntaxException e)
		{
			System.out.println(e.getMessage());
		}
	}

	private static void listAlbums()
	{
		HttpGet request = new HttpGet(ALBUM_SERVICE_API);
		try
		{
			HttpResponse response = client.execute(request);
			if (response.getEntity() != null) response.getEntity().getContent().transferTo(System.out);
			System.out.println();
		}
		catch (IOException e)
		{
			System.out.println(e.getMessage());
		}
	}

	private static void getAlbum()
	{
		System.out.print("> Album's ISRC : ");
		String isrc = scanner.nextLine();

		try
		{
			URIBuilder uriBuilder = new URIBuilder(ALBUM_SERVICE_API);
			uriBuilder.setPath(uriBuilder.getPath()+"/"+isrc);
			HttpGet request = new HttpGet(uriBuilder.build());

			HttpResponse response = client.execute(request);
			if (response.getEntity() != null) response.getEntity().getContent().transferTo(System.out);
			System.out.println();
		}
		catch (IOException | URISyntaxException e)
		{
			System.out.println(e.getMessage());
		}
	}

	private static void addAlbum()
	{
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode rootNode = mapper.createObjectNode();

		System.out.print("> Album's ISRC : ");
		rootNode.put("isrc", scanner.nextLine());

		System.out.print("> Album's title : ");
		rootNode.put("title", scanner.nextLine());

		System.out.print("> Album's description : ");
		rootNode.put("description", scanner.nextLine());

		System.out.print("> Album's releaseYear : ");
		rootNode.put("releaseYear", scanner.nextLine());

		System.out.print("> Album's artistNickname : ");
		rootNode.put("artistNickname", scanner.nextLine());

		try
		{
			HttpPost request = new HttpPost(ALBUM_SERVICE_API);
			request.setEntity(new StringEntity(mapper.writeValueAsString(rootNode)));
			request.setHeader("Content-type", "application/json");

			HttpResponse response = client.execute(request);
			if (response.getEntity() != null) response.getEntity().getContent().transferTo(System.out);
			System.out.println();
		}
		catch (IOException e)
		{
			System.out.println(e.getMessage());
		}
	}

	private static void updateAlbum()
	{
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode rootNode = mapper.createObjectNode();

		System.out.print("> Album's ISRC : ");
		String isrc = scanner.nextLine();
		if (!isrc.isBlank()) rootNode.put("isrc", isrc);

		System.out.print("> Album's title (ENTER to skip): ");
		String title = scanner.nextLine();
		if (!title.isEmpty()) rootNode.put("title", title);

		System.out.print("> Album's description (ENTER to skip): ");
		String description = scanner.nextLine();
		if (!description.isEmpty()) rootNode.put("description", description);

		System.out.print("> Album's releaseYear (ENTER to skip): ");
		String releaseYear = scanner.nextLine();
		if (!releaseYear.isEmpty()) rootNode.put("releaseYear", releaseYear);

		System.out.print("> Album's artistNickname (ENTER to skip): ");
		String artistNickname = scanner.nextLine();
		if (!artistNickname.isEmpty()) rootNode.put("artistNickname", artistNickname);

		try
		{
			HttpPut request = new HttpPut(ALBUM_SERVICE_API);
			request.setEntity(new StringEntity(mapper.writeValueAsString(rootNode)));
			request.setHeader("Content-type", "application/json");

			HttpResponse response = client.execute(request);
			if (response.getEntity() != null) response.getEntity().getContent().transferTo(System.out);
			System.out.println();
		}
		catch (IOException e)
		{
			System.out.println(e.getMessage());
		}
	}

	private static void deleteAlbum()
	{
		System.out.print("> Album's ISRC : ");
		String isrc = scanner.nextLine();

		try
		{
			URIBuilder uriBuilder = new URIBuilder(ALBUM_SERVICE_API);
			uriBuilder.setPath(uriBuilder.getPath()+"/"+isrc);
			HttpDelete request = new HttpDelete(uriBuilder.build());

			HttpResponse response = client.execute(request);
			if (response.getEntity() != null) response.getEntity().getContent().transferTo(System.out);
			System.out.println();
		}
		catch (IOException | URISyntaxException e)
		{
			System.out.println(e.getMessage());
		}
	}
}
