package client;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class Menu
{
	private Menu previousMenu;
	private List<MenuOption> options;
	private Scanner scanner;
	
	public Menu()
	{
		this(null);
	}
	
	public Menu(Menu previous)
	{
		setPreviousMenu(previous);
		setOptions(new ArrayList<MenuOption>());
		scanner = new Scanner(System.in);
	}
	
	public void AddOption(String optionName, Runnable action)
	{
		options.add(new MenuOption(optionName, action));
	}
	
	public void prompt()
	{
		int choice = -1;
		while (choice != 0)
		{
			render();
			
			try
			{
				if (scanner.hasNext() && scanner.hasNextInt())
					choice = scanner.nextInt();
				else
					throw new InputMismatchException();
				
				if (choice < 0 || choice > options.size()) 
					throw new IndexOutOfBoundsException();
				else if (choice != 0)
					options.get(choice-1).getAction().run();
			}
			catch(InputMismatchException | IndexOutOfBoundsException e)
			{
				System.out.println("Error. Please enter a valid input.");
				scanner.next();
			}
		}
	}
	
	private void render()
	{
		System.out.println();
		for (int i = -1; i < options.size(); ++i)
		{
			if (i < 0)
				System.out.println(previousMenu == null ? "0. Quit" : "0. Cancel");
			else
				System.out.println(String.format("%d. %s", i+1, options.get(i).getName()));
		}
		
		System.out.print("\n> ");
	}

	public List<MenuOption> getOptions()
	{
		return options;
	}
	public void setOptions(List<MenuOption> options)
	{
		this.options = options;
	}
	
	public Menu getPreviousMenu()
	{
		return previousMenu;
	}

	public void setPreviousMenu(Menu previousMenu)
	{
		this.previousMenu = previousMenu;
	}

	public class MenuOption
	{
		private String name;
		private Runnable action;
		
		public MenuOption(String optionName, Runnable action)
		{
			this.name = optionName;
			this.action = action;
		}
		
		public String getName()
		{
			return name;
		}
		public void setName(String name)
		{
			this.name = name;
		}
		
		public Runnable getAction()
		{
			return action;
		}
		public void setAction(Runnable action)
		{
			this.action = action;
		}
	}
}
